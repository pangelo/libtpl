
libTPL - an embeddable, pure C implementation of the Tool Path Language
==================

libTPL is an expressive set of C library functions to describe machine tool paths. These paths are normally used to drive digital fabrication tools like CNCs, laser cutters and 3D printers, and are usually expressed in [GCode], a very simple direct encoding of machine movements.

The library tries to follow as close as possible the API of the original [Tool Path Language] and aims to make it easy to create scripting language bindings that help separate tool path description (e.g. in a high level scripting language like Python) from tool path generation.

Building
--------

Building the library requires only Make and a C11 compiler, and currently only a static library is built. On Linux with GCC installed you only need to run `make` at the top-level source directory.

Using
-----

The `src/tpl.h` header and the files in the `test` folder are currently the best reference on using the library. 

Contributing
------------

All contributions are welcome, especially:

 * testing and porting the library on platforms and compilers othe than Linux and GCC.
 * creating language bindings for the library (e.g. Python)
 * using the library in applications

the [TODO](./TODO.md) file is used for tracking pending development tasks.

[Tool Path Language]: (https://tplang.org)

[GCode]: https://linuxcnc.org/docs/html/gcode/g-code.html
