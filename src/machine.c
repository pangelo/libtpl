#include "tpl.h"
#include "_tpl_internal.h"

//
// Functions that handle machine state
//

tpl_status_t
tpl_feed_set (tpl_context_t ctx, float rate, tpl_feed_mode_t mode)
{
  return _tpl_not_implemented (ctx, "tpl_feed_set()");
}

float
tpl_feed_get (tpl_context_t ctx)
{
  _tpl_not_implemented (ctx, "tpl_feed_set()");
  return 0.0;
}
