#include <stdlib.h>
#include <string.h>

#include "tpl.h"
#include "_tpl_internal.h"

tpl_context_t
tpl_create_context()
{
  tpl_context_t new_context = (tpl_context_t) malloc (sizeof (struct _tpl_context_data));

  _tpl_set_error_state(new_context, false, "No Error");

  return new_context;
}
