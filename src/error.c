#include <stdlib.h>
#include <string.h>

#include "tpl.h"
#include "_tpl_internal.h"

void
_tpl_set_error_state (tpl_context_t ctx, bool new_state, const char *description)
{
  ctx->error_state = new_state;
  strncpy (ctx->error_str, description, MAX_ERROR_STR);
}

tpl_status_t
_tpl_not_implemented (tpl_context_t ctx, const char* feature_desc)
{
  size_t feature_len = strlen (feature_desc);
  char *fixed_desc = "libTPL not implemented: ";
  int fixed_len = strlen (fixed_desc);
  int desc_len = fixed_len + feature_len + 1;

  char *description = (char *) malloc (desc_len);
  description[0] = '\0';
  strncat (description, fixed_desc, fixed_len);
  strncat (description, feature_desc, feature_len);

  _tpl_set_error_state(ctx, false, description);

  free(description);

  return TPL_STATUS_ERROR;
}

bool
tpl_has_error (tpl_context_t ctx) 
{
  return ctx->error_state;
}

const char*
tpl_get_last_error (tpl_context_t ctx)
{
  return ctx->error_str;
}
