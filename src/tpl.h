#ifndef TPL_H
#define TPL_H

#include <stdbool.h>

//
// LIBRARY TYPES
//

//
// The `tpl_context_t` struct is an opaque datatype that stores the
// current "machine state" and is required by all libTPL functions.
//

typedef struct _tpl_context_data * tpl_context_t;

//
// All libTPL functions return a `tpl_status_t` error flag.
// In case of an error, the function `tpl_get_last_error()`
// can be used to get a string description of the error
//

typedef enum {
  TPL_STATUS_ERROR       = -1,
  TPL_STATUS_OK          =  0
} tpl_status_t;

// Possible modes for the `tpl_set_feed()` function

typedef enum {
  TPL_FEED_UNITS_PER_MIN =  2,
  TPL_FEED_INVERSE_TIME  =  3,
  TPL_FEED_UNITS_PER_REV =  4
} tpl_feed_mode_t;

//
// LIBRARY INITIALIZATION
//

// Before using any libTPL functions we should create a context object

tpl_context_t tpl_create_context();


//
// MACHINE STATE
//

// Check if the machine is in an error state
bool tpl_has_error (tpl_context_t ctx);

// Get a string pointer to a textual description of the latest error
const char* tpl_get_last_error(tpl_context_t ctx);


// Get/set the current machine feed rate (tool travel speed)
//
// `rate` indicates the units per minute in the XYZ Cartesian system of
// all subsequent non-rapid moves until the feed rate is changed.
//
// mode may be one of the following:
//
// - FEED_UNITS_PER_MIN: the feed rate is in units per minute.
//   The unit may be inches, millimeters or degrees depending on the
//   current unit of length and which axes are moving.
//
// - FEED_INVERSE_TIME: indicates that moves should be completed in
//   one divided by rate minutes. For example, if rate is 2.0, moves
//   should be completed in half a minute.
//
// - FEED_UNITS_PER_REV: means that the controlled point should move
//   a certain number of units per revolution.

tpl_status_t tpl_feed_set (tpl_context_t ctx, float rate, tpl_feed_mode_t mode);

float tpl_feed_get (tpl_context_t ctx);

#endif // TPL_H
