#ifndef _TPL_INTERNAL_H
#define _TPL_INTERNAL_H

#include <stdbool.h>

//
// This header describes internal data structures and functions used
// by the library.
//

#define MAX_ERROR_STR 1024

struct _tpl_context_data {
  bool error_state;
  char error_str[MAX_ERROR_STR];
};

// helper function to set the error state fields in one go
void _tpl_set_error_state (tpl_context_t ctx, bool new_state, const char *description);

// helper function to set a proper error state for not implemented functions
tpl_status_t _tpl_not_implemented (tpl_context_t ctx, const char* feature_desc);

#endif // TPL_INTERNAL_H
