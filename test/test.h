#ifndef TEST_H
#define TEST_H

#include <stdbool.h>
#include <tpl.h>


void check (bool assertion, const char *test_description);

void check_tpl_error (tpl_context_t ctx, const char *test_description);

void check_report();

#endif