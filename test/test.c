
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "test.h"

const size_t MAX_BUF = 1024;
static unsigned int test_num = 0;
static unsigned int test_pass = 0;

void 
check (bool assertion, const char *test_description) 
{
    const char *result = "not ok";
    if (assertion) {
        result = "ok";
        test_pass++;
    }
    
    printf ("%s %d %s\n", result, ++test_num, test_description);
}

void 
check_tpl_error (tpl_context_t ctx, const char *test_description) 
{
    const char *error_description = tpl_get_last_error(ctx);
    const char *full_description = test_description;
    char *buf = NULL;
    bool test_error = tpl_has_error(ctx);
    if (test_error) {
        int buf_len = strlen(test_description) + strlen(" : ") + strlen(error_description);
        buf = (char *) malloc (buf_len + 1);
        strcat (buf, test_description);
        strcat (buf, " : ");
        strcat (buf, error_description);
        full_description = buf;
    }

    check (!test_error, full_description);
    if (buf != NULL) free(buf);
}

void 
check_report()
{
    printf ("%d out of %d tests passed\n", test_pass, test_num);
}