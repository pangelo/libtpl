#include <stdio.h>
#include "tpl.h"
#include "test.h"

int main(int argc, char** argv) {

  tpl_context_t machine = tpl_create_context();
  check_tpl_error (machine, "context creation");

  float test_feed_rate = 100.0;
  tpl_status_t status = tpl_feed_set (machine, test_feed_rate, TPL_FEED_UNITS_PER_MIN);
  
  check (status == TPL_STATUS_OK, "setting feed rate");
  check (tpl_feed_get(machine) == test_feed_rate, "getting feed rate");

  check_report();

  return 0;
}
