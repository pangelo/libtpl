
SOURCE_DIR = src
TEST_DIR = test
OBJ_DIR = build/obj
BIN_DIR = build/bin
LIB_DIR = build/lib

LIB_SOURCES = $(wildcard $(SOURCE_DIR)/*.c)
LIB_OBJS = $(patsubst %.c,$(OBJ_DIR)/lib/%.o,$(notdir $(LIB_SOURCES)))
LIB_TARGET = $(LIB_DIR)/libtpl.a
LIB_INCLUDE = $(SOURCE_DIR)
LIB_CFLAGS = -std=c11 -g -Wall

TEST_TARGET = $(BIN_DIR)/run_tests
TEST_SOURCES = $(wildcard $(TEST_DIR)/*.c)
TEST_OBJS = $(patsubst %.c,$(OBJ_DIR)/test/%.o,$(notdir $(TEST_SOURCES)))
TEST_CFLAGS = -std=c11 -g -Wall

all: $(LIB_TARGET)

$(LIB_TARGET): $(LIB_OBJS)
	ar rcs $(LIB_TARGET) $(LIB_OBJS)

$(OBJ_DIR)/lib/%.o: $(SOURCE_DIR)/%.c
	gcc -c $(LIB_CFLAGS) -I $(LIB_INCLUDE) -o $@ $<

clean:
	-rm $(LIB_TARGET)
	-rm $(LIB_OBJS)
	-rm $(TEST_TARGET)
	-rm $(TEST_OBJS)

$(OBJ_DIR)/test/%.o: $(TEST_DIR)/%.c
	gcc -c $(TEST_CFLAGS) -I $(LIB_INCLUDE) -o $@ $<

test: $(TEST_TARGET)
	@$(TEST_TARGET)

$(TEST_TARGET): $(TEST_OBJS) $(LIB_TARGET)
	gcc $(TEST_CFLAGS) -I $(LIB_INCLUDE) -o $@ $^

.PHONY: clean test
